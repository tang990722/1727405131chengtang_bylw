#-*-encoding:utf-8-*-
import csv
import jieba
f = open("cleanedData.csv","r",encoding='utf-8')
reader = csv.reader(f)
mapp = {}
for row in reader :
    if(row[0] in mapp.keys()):
        mapp[row[0]]+=[row[1]]
    else:
        mapp[row[0]]=[row[1]]
for key in mapp.keys():
    values = mapp[key]
    for value in values:
        value = " ".join(jieba.lcut(value))
        print(value)
