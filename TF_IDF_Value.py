from nltk.text import TextCollection

#sinica_text = nltk.Text(pcrText.swords())               #pcrText.words()返回所加载文档的所有词汇
sinica_text = nltk.Text(pcrText.words('comment4.txt'))  #pcrText.words()返回comment4.txt的所有词汇
mytexts = TextCollection(pcrText)                       #TextCollection()用于返回一个文档集合
 
len(mytexts._texts)                                     #表示文档集合里面包含的文档个数
len(mytexts)                                            #表示文档集合里面包含的词汇个数
 
the_set = set(sinica_text)                              #去除文档中重复的词汇，从而形成词汇表。
len(the_set)
for tmp in the_set:
    print(tmp, "[tf]", mytexts.tf(tmp, pcrText.raw(['comment4.txt'])), "[idf]", mytexts.idf(tmp),"【tf_idf】", mytexts.tf_idf(tmp, pcrText.raw(['comment4.txt'])))
#pcrText.raw(['comment4.txt'])用于返回对应文章的所有内容，以便计算tf和tf_idf值。
#通过tf,idf,tf_idf这三个函数来计算每个词汇在语料库以及对应文章中的值
