def process(data):     
    content = data.replace(' ','')   # 去掉文本中的空格和省略号
    content1 = content.replace(' ','')    
    content2 = content1.replace('...',',')
    return content
def clear_character(sentence):    
    pattern = re.compile("[^\u4e00-\u9fa5^,^.^!^a-z^A-Z^0-9]")  #只保留中英文、数字和符号，去掉其他东西
    #若只保留中英文和数字，则替换为[^\u4e00-\u9fa5^a-z^A-Z^0-9]
    line=re.sub(pattern,'',sentence)  #把文本中匹配到的字符替换成空字符
    new_sentence=''.join(line.split())    #去除空白
    return new_sentence

def dataClean(data):
    processedData = process(data)
    cleanedData = clear_charater(processedData)
    return cleamnedData

